<?php namespace Herbert\Framework\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Herbert\Framework\Models\SoftDeletes\SoftDeletes;

/**
 * @see http://getherbert.com
 */
class User extends Model {


    /**
     * Disable timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';




    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_login', 'user_pass',
        'user_nicename', 'user_email', 'user_url',
        'user_registered', 'user_activation_key', 'user_status',
        'display_name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'user_registered'
    ];


    /**
     * UserMeta relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function meta()
    {
        return $this->hasMany(__NAMESPACE__ . '\UserMeta', 'user_id');
    }




}
