<?php namespace Herbert\Framework\Providers;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\TwigFunction;

/**
 * @see http://getherbert.com
 */
class TwigServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('twig.loader', function () {

            // the path to TwigBridge library so Twig can locate the
            // form_div_layout.html.twig file
            $appVariableReflection = new \ReflectionClass('\Symfony\Bridge\Twig\AppVariable');
            $vendorTwigBridgeDirectory = dirname($appVariableReflection->getFileName());
            // the path to your other templates
            $viewsDirectory = realpath(__DIR__ . '/../../../../../../resources/views');
            $loader = new \Twig\Loader\FilesystemLoader([
                $viewsDirectory,
                $vendorTwigBridgeDirectory . '/Resources/views/Form',
            ]);

            foreach ($this->app->getPlugins() as $plugin) {
                $loader->addPath($plugin->getBasePath() . '/views', $plugin->getTwigNamespace());
            }

            //$loader->addPath($plugin->getBasePath() . '/views', $plugin->getTwigNamespace());

            return $loader;
        });

        $this->app->bind('twig.options', function () {
            return [
                'debug' => $this->app->environment() === 'local',
                'charset' => 'utf-8',
                //'cache' => content_directory() . '/twig-cache',
                'cache' => __DIR__ . '/../../../../../../var/twig-cache',
                'auto_reload' => true,
                'strict_variables' => false,
                'autoescape' => 'html',
                'optimizations' => -1
            ];
        });

        $this->app->bind('twig.functions', function () {
            return [
                'dd',
                'herbert',
                'view',
                'content_directory',
                'plugin_directory',
                'panel_url',
                'route_url',
                'session',
                'session_flashed',
                'errors'
            ];
        });

        $this->app->singleton('twig', function () {
            return $this->constructTwig();
        });

        $this->app->alias(
            'twig',
            'Twig_Environment'
        );
    }

    /**
     * Constructs Twig.
     *
     * @return \Twig\Environment
     */
    public function constructTwig()
    {

        $twig = new Environment($this->app['twig.loader'], $this->app['twig.options']);

        if ($this->app->environment() === 'local') {
            $twig->addExtension(new DebugExtension);
        }

        foreach ($this->app->getViewGlobals() as $key => $value) {
            $twig->addGlobal($key, $value);
        }

        $twig->addGlobal('errors', $this->app['errors']);

        foreach ((array)$this->app['twig.functions'] as $function) {
            $twig->addFunction(new TwigFunction($function, $function));
        }

        $session = new \Symfony\Component\HttpFoundation\Session\Session();

        $csrfGenerator = new \Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator();
        $csrfStorage = new \Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage($session);
        $csrfManager = new \Symfony\Component\Security\Csrf\CsrfTokenManager($csrfGenerator, $csrfStorage);

        if (realpath(__DIR__ . '/../../../../../../resources/config/config.yaml') and $values = Yaml::parseFile(__DIR__ . '/../../../../../../resources/config/config.yaml') and is_array($values)
            and isset($values['twig']) and isset($values['twig']['form_theme'])) {
            $defaultFormTheme = $values['twig']['form_theme'];
        } elseif (realpath(__DIR__ . '/../Resources/config/twig.yaml') and $values = Yaml::parseFile(__DIR__ . '/../Resources/config/twig.yaml') and is_array($values)
            and isset($values['twig']) and isset($values['twig']['form_theme'])) {
            $defaultFormTheme = $values['twig']['form_theme'];
        } else {
            $defaultFormTheme = 'form_div_layout.html.twig';
        }

        //$vendorDirectory = realpath(__DIR__ . '/../../../../../../vendor');
        $formEngine = new \Symfony\Bridge\Twig\Form\TwigRendererEngine([$defaultFormTheme], $twig);
        $twig->addRuntimeLoader(new \Twig\RuntimeLoader\FactoryRuntimeLoader([
            \Symfony\Component\Form\FormRenderer::class => function () use ($formEngine, $csrfManager) {
                return new \Symfony\Component\Form\FormRenderer($formEngine, $csrfManager);
            },
        ]));

        // ... (see the previous CSRF Protection section for more information)

        // adds the FormExtension to Twig
        $twig->addExtension(new \Symfony\Bridge\Twig\Extension\FormExtension());

        $translator = new \Symfony\Component\Translation\Translator('fr');
        // somehow load some translations into it
        $translator->addLoader('yaml', new \Symfony\Component\Translation\Loader\YamlFileLoader());
        $translator->addResource(
            'yaml',
            __DIR__.'/../../../../../../resources/translations/messages.fr.yaml',
            'fr'
        );

        // adds the TranslationExtension (it gives us trans filter)
        $twig->addExtension(new \Symfony\Bridge\Twig\Extension\TranslationExtension($translator));


        return $twig;
    }

}
